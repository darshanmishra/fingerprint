<?php
    require_once 'lib/_config.php'; 
	date_default_timezone_set('Asia/kolkata');
	$date =  date("Y-m-d"); 	
	$time =  date("h:i:s"); 	
?>
<!DOCTYPE html>
<html>
	  <head>
		<title> Attendence Record</title>
	  </head>
  <body>
<?php
  if($_GET["function"]=="read")
	{
?>
  <table border=1 cellpadding=10>
		<thead>
		  <th>Sl No.</th>
		  <th>Name</th>
		  <th>Time</th>
		  <th>Date</th>
		  <th>Action</th>
		</thead>
		<tbody>
<?php
			$cont = 1;
			$sql = $link->query("SELECT * FROM `employee` ORDER BY `fingerprint_id` ASC");
			while($rows = $sql->fetch())
				{
					$sql1 = $link->query("SELECT * FROM `attendence` WHERE `fingerprint_id` = '".$rows['fingerprint_id']."' AND `date` ='".$date."'  ORDER BY `id` DESC LIMIT 0,1");
					$rows1 = $sql1->fetch();
					
						if($rows['fingerprint_id']==$rows1['fingerprint_id'])
						{	
							$name = $rows['name']; 
								if($rows1['action']=="1")
									{
										$action = "Sign In"; 
									} 
								else
									{
										$action = "Sign Out";
									}
?>
					<tr>
						<td><?php echo $cont; ?></td>
						<td><a href='attendence2.php?function=history&id=<?php echo $rows1['fingerprint_id']; ?>&name=<?php echo $rows['name']; ?>' target="_blank"><?php echo $name; ?></a></td>
						<td><?php echo $rows1['time']; ?></td>
						<td><?php echo $rows1['date']; ?></td>
						<td><?php echo $action; ?></td>
					</tr>
<?php
		$cont++;
						}
				}
?>
		</tbody>
</table>
<?php
	}

    if($_GET["function"]=="history")
		{
?>
		RECORD OF <?php echo $_GET['name']; ?>
		<table border=1 cellpadding=10>
			<thead>
				<th>Sl no.</th>
				<th>Date</th>
				<th>In</th>
				<th>Out</th>
				<th>Duration</th>
				<th>Attendence</th>
			</thead>
			<tbody>
<?php
		  
			$a = 1;
			$cont = 1;
			$dates = [];
			$sql = $link->query("SELECT DISTINCT `date` FROM `attendence` WHERE `fingerprint_id` = '".$_GET['id']."' ");
			while($rows = $sql->fetch())
				{		
					$time_difference = '00:00:00';
					$time_difference1 = '00:00:00';
?>
						<tr>
							<td><?php echo $cont; ?></td>
							<td><?php echo $rows['date']; ?></td>
<?php
							$sql1 = $link->query("SELECT * FROM `attendence` WHERE `fingerprint_id` = '".$_GET['id']."' AND `date` = '".$rows['date']."' ");
							while($rows1 = $sql1->fetch())
								{	
									if($rows1['action'] == 1   && ($a%2)!= 0)
										{
										  $x = $rows1['time'];
										  $a++;					 
										}
									else if($rows1['action'] == 1 && (($a%2)!= 1 || ($a%2)== 1 ))
										{
										  $x = $rows1['time'];
											$a++; 
										}
									else if($rows1['action'] == 0 && (($a%2)== 0 || ($a%2)!= 0))
										{
											$y = $rows1['time'];
											$time_difference = get_time_difference($x, $y);
											$time_difference = strtotime($time_difference)-strtotime("00:00:00");
											$time_difference1 = date("H:i:s",strtotime($time_difference1)+$time_difference);
											echo "<br />";
											$a++;
										}
								}
						
			$sql2 = $link->query("SELECT * FROM `attendence` WHERE `fingerprint_id` = '".$_GET['id']."' AND `date` = '".$rows['date']."' ORDER BY `id` ASC LIMIT 0,1");
			$rows2 = $sql3->fetch();
			$sign_in = $rows2['time'];
			
			$sql3 = $link->query("SELECT * FROM `attendence` WHERE `fingerprint_id` = '".$_GET['id']."' AND `date` = '".$rows['date']."'  ORDER BY `id` DESC LIMIT 0,1");
			$rows3 = $sql3->fetch();
			if($rows3['action'] == '0')
				{
					$sign_out = $rows3['time'];
					$attendence = '&#9745';
				}
			else
				{
					$sign_out = 'Sign In';
					$attendence = '&#9744';
				}	
?>
							<td><?php echo $sign_in; ?></td>
							<td><?php echo $sign_out; ?></td>
							<td><?php echo $time_difference1; ?></td>
							<td><?php echo $attendence; ?></td>
						</tr>	
<?php		
				  $cont++;
				}
?>
		  </tbody>
	</table>
<?php
		}
  
  
	if($_GET["function"]=="create")
		{
			$sql1 = $link->query("SELECT * FROM `attendence` WHERE `fingerprint_id` = '".$_GET['fingerprint_id']."' AND `date` ='".$date."'  ORDER BY `id` DESC LIMIT 0,1");
			$rows1 = $sql1->fetch();
	
				if(!$rows1 || $rows1['action'] ==0)
					{
						$action = 1; 
					} 
				else
					{
						$action = 0;
					}

			$sql = $link->query("INSERT INTO `attendence`(`fingerprint_id`, `time`, `date`, `action`) VALUES ('".$_GET['fingerprint_id']."','".htmlentities(trim(addslashes(strip_tags($time))))."','".htmlentities(trim(addslashes(strip_tags($date))))."','".htmlentities(trim(addslashes(strip_tags($action))))."')");
				if($sql)
						echo $msg = "<small style=\"color:#10a5d5; font-size:14px;\">Successfully Saved</small>";
				else
						echo $msg = "<small style=\"color:#d35900; font-size:14px;\">Unsuccessfully Saved</small>";	
		}
?>  
 
  </body>
</html>

<?php

	function get_time_difference($time1, $time2) {
		$time1 = strtotime("$date $time1");
		$time2 = strtotime("$date $time2");
		if ($time2 < $time1) 
			{
				$time2 += 86400;
			}
		return date("H:i:s", strtotime("1980-01-01 00:00:00") + ($time2 - $time1));
	}


?>